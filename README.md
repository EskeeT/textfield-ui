# TextField UI

npm i - для установки необходимых компонентов
npm start - для запуска проекта
npm run build - для создания папки build

onChange, //для изменения значения
label, //верхний лейбл
placeholder, //подсказывающий текст
caption, //нижний леюл
required, // флаг который показывает обязательло ли заполнять
failed, // флаг который подсвечивает красным textField
value, // значение
limitedSymbols, //максимальный размер символов для ввода
loading, //флаг который показывает иконку загрузки
disabled, //флаг который блокирует textField
size, // размер textField s/m/l
variant, // вариант textfield primary/secondary
fullWidth, //флаг который растягивает textField на всю ширину
multiline, //флаг который при переполнении textField расширяется вниз
readOnly, // флаг только для чтения
id, //id элемента
