import App from 'components/App/App';
import * as ReactDOM from 'react-dom';

ReactDOM.render(
  <App />,
  // eslint-disable-next-line
  document.getElementById('root') as HTMLElement
);
