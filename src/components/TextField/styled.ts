import styled from 'styled-components';
import { TSize } from './types';

export const RequiredStyled = styled.span({
  color: '#E31227',
});

export const FlexBox = styled.div<{ isCaption: boolean; isSecondary: boolean }>(
  ({ isCaption, isSecondary }) => ({
    display: 'flex',
    justifyContent: isCaption ? 'space-between' : 'flex-end',
    padding: isSecondary ? '0 10px' : 0,
  }),
);

export const CaptionWrapper = styled.span({
  wordWrap: 'break-word',
  overflow: 'hidden',
});

export const SymbolsCountWrapper = styled.span({
  marginLeft: '15px',
});

export const LoaderWrapper = styled.div({
  height: '35px',
  width: '35px',
  marginRight: '5px',
});

const variantPadding = (size?: TSize) => {
  switch (size) {
    case 'l':
      return '14px 10px';
    case 's':
      return '6px 10px';
    case 'm':
    default:
      return '10px';
  }
};

export const SizeWrapper = styled.div<{ size?: TSize }>(({ size }) => ({
  input: { padding: variantPadding(size) },
}));
