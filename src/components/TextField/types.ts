export type TSize = 's' | 'm' | 'l';

export interface ITextField {
  label?: string;
  placeholder?: string;
  caption?: string;
  required?: boolean;
  failed?: boolean;
  value: string;
  limitedSymbols?: number;
  loading?: boolean;
  disabled?: boolean;
  size?: TSize;
  variant?: 'primary' | 'secondary';
  fullWidth?: boolean;
  multiline?: boolean;
  readOnly?: boolean;
  onChange?: (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => void;
  id: string;
}
