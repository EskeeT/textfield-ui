import { FormControl, InputAdornment, InputLabel } from '@material-ui/core';
//import TextField from '@material-ui/core/TextField';

import InputBase from '@material-ui/core/InputBase';

import { createStyles, Theme, withStyles, makeStyles } from '@material-ui/core/styles';
import { ITextField } from './types';
import {
  RequiredStyled,
  FlexBox,
  CaptionWrapper,
  SymbolsCountWrapper,
  LoaderWrapper,
  SizeWrapper,
} from './styled';
import Loader from './Loader/Loader';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    margin: {
      margin: theme.spacing(1),
    },
    primary: {
      borderRadius: '8px',
      border: '1px solid #B2B2B2',
      transition: '.1s',
      boxSizing: 'border-box',

      '&:hover': {
        borderColor: '#707070',
      },
      '&.Mui-focused': {
        borderColor: '#148F2A',
        outline: '1px solid #148F2A',
      },
      '&.Mui-error': {
        borderColor: '#E31227',
        outline: 'none',
      },
    },
    secondary: {
      border: 'none',
      borderBottom: '2px solid #B2B2B2',
      transition: '.1s',
      '&:hover': {
        borderColor: '#707070',
      },
      '&.Mui-focused': {
        borderBottom: '2px solid #148F2A',
      },
      '&.Mui-error': {
        borderColor: '#E31227',
      },
    },
  }),
);

const CaptionStyled = withStyles({
  root: {
    color: '#4E4E4E',
    fontSize: '12px',
    lineHeight: '16px',
    position: 'relative',
    marginTop: '4px',
    width: '100%',
    transform: 'none',
    '&.Mui-focused': {
      color: '#4E4E4E',
    },
    '&.Mui-error': {
      color: '#E31227',
    },
  },
})(InputLabel);

const Input = withStyles(() =>
  createStyles({
    root: {
      'label + &': {
        marginTop: '20px',
      },
      position: 'relative',
    },
    input: {
      color: '#262626',
      height: '18px',
      font: 'Roboto',
      fontSize: 16,
      width: '100%',
    },
  }),
)(InputBase);

const LabelStyled = withStyles({
  root: {
    color: '#4E4E4E',
    transform: 'none',
    fontSize: '12px',
    lineHeight: '16px',
    '&.Mui-focused': {
      color: '#4E4E4E',
    },
  },
})(InputLabel);

const TextField: React.FC<ITextField> = ({
  onChange, //для изменения значения
  label, //верхний лейбл
  placeholder, //подсказывающий текст
  caption, //нижний леюл
  required, // флаг который показывает обязательло ли заполнять
  failed, // флаг который подсвечивает красным textField
  value, // значение
  limitedSymbols, //максимальный размер символов для ввода
  loading, //флаг который показывает иконку загрузки
  disabled, //флаг который блокирует textField
  size, // размер textField s/m/l
  variant, // вариант textfield primary/secondary
  fullWidth, //флаг который растягивает textField на всю ширину
  multiline, //флаг который при переполнении textField расширяется вниз
  readOnly, // флаг только для чтения
  id, //id элемента
}) => {
  const countSumbols: number = value.length;

  const classes = useStyles();

  return (
    <SizeWrapper size={size}>
      <FormControl style={{ width: fullWidth ? '100%' : 'auto', opacity: disabled ? '48%' : 1 }}>
        {label && (
          <LabelStyled shrink htmlFor={id}>
            {label}
            {required && <RequiredStyled>*</RequiredStyled>}
          </LabelStyled>
        )}
        <Input
          readOnly={readOnly}
          multiline={multiline}
          className={variant === 'secondary' ? classes.secondary : classes.primary}
          onChange={(event) => limitedSymbols && countSumbols < limitedSymbols && onChange?.(event)}
          disabled={disabled}
          value={value}
          error={failed}
          required={required}
          placeholder={placeholder}
          spellCheck={false}
          id={id}
          endAdornment={
            loading && (
              <InputAdornment position="end">
                <LoaderWrapper>
                  <Loader />
                </LoaderWrapper>
              </InputAdornment>
            )
          }
        />
        {(caption || limitedSymbols) && (
          <CaptionStyled error={failed} shrink htmlFor={id}>
            <FlexBox isCaption={caption !== undefined} isSecondary={variant === 'secondary'}>
              {caption && <CaptionWrapper>{caption}</CaptionWrapper>}
              {limitedSymbols && (
                <SymbolsCountWrapper>{`${countSumbols}/${limitedSymbols}`}</SymbolsCountWrapper>
              )}
            </FlexBox>
          </CaptionStyled>
        )}
      </FormControl>
    </SizeWrapper>
  );
};

export default TextField;
