import styled from 'styled-components';

export const Container = styled.div({
  position: 'relative',
  width: '100%',
  height: '100%',
  overflow: 'hidden',
  color: 'black',
});

export const AnimategWrapper = styled.div({
  position: 'absolute',
  top: 'calc(50% - 12px)',
  left: 'calc(50% - 12px)',
  width: 24,
  height: 24,
  lineHeight: 0,
  animation: 'anim 1s cubic-bezier(0.70, 0.45, 0.45, 0.70) infinite',

  '@keyframes anim': {
    '0%': {
      transform: 'rotate(200deg)',
    },
    '100%': {
      transform: 'rotate(550deg)',
    },
  },
});

// export const BlurWrapper = styled.div({
//   filter: 'blur(10px)',
//   opacity: 0.5,
// });
