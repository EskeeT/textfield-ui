import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle({
  body: {
    color: 'black',
    background: 'white',
    font: '16px Roboto',
    width: '100%',
    height: '100%',
    margin: '0',
    padding: '0',
    cursor: 'default',
  },

  '*': {
    boxSizing: 'border-box',
  },

  a: {
    textDecoration: 'none',
  },

  'input, button, select, textarea': {
    font: '16px Roboto',
    letterSpacing: '0.75px',
  },
});
