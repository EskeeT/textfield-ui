//import React from 'react';
import ApplicationPage from '../ApplicationPage/ApplicationPage';
import { GlobalStyle } from './styled';

const App = () => {
  return (
    <>
      <GlobalStyle />
      <ApplicationPage />
    </>
  );
};

export default App;
