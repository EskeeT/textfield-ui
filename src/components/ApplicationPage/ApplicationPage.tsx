import React, { useState } from 'react';
import { Container, Wrapper, Box } from './styled';
import TextField from 'components/TextField/TextField';

const ApplicationPage: React.FC = () => {
  const [value, setValue] = useState('');

  return (
    <Container>
      <h2>Primary</h2>
      <Wrapper>
        <Box>
          <TextField
            readOnly
            value={'ReadOnly'}
            label="Text"
            caption="variant=primary; readOnly=true"
            size="s"
            id="textfield-1"
          />
        </Box>
        <Box>
          <TextField
            label="Text"
            placeholder="placehholder Text"
            loading
            caption="variant=primary; loading=true; readOnly=true"
            limitedSymbols={250}
            value={'TextTextTextTextTextTextTextTextTextText'}
            readOnly
            size="m"
            id="textfield-2"
          />
        </Box>
        <Box>
          <TextField
            value={'TextTextTextTextTextTextTextTextTextText'}
            label="Text"
            placeholder="privet"
            limitedSymbols={5000}
            size="l"
            id="textfield-3"
          />
        </Box>
      </Wrapper>
      <Wrapper>
        <Box style={{ alignItems: 'center', margin: '0' }}>
          <h3>Size: s</h3>
        </Box>
        <Box style={{ alignItems: 'center', margin: '0' }}>
          <h3>Size: m</h3>
        </Box>
        <Box style={{ alignItems: 'center', margin: '0' }}>
          <h3>Size: l</h3>
        </Box>
      </Wrapper>
      <Wrapper>
        <Box>
          <TextField
            value={value}
            onChange={(event) => setValue(event.target.value)}
            caption="variant=primary; failed=true; size=s disabled=true required=true"
            failed
            required
            label="Text"
            placeholder="disabled"
            limitedSymbols={5000}
            size="s"
            disabled
            id="textfield-4"
          />
        </Box>
        <Box>
          <TextField
            value={value}
            onChange={(event) => setValue(event.target.value)}
            failed
            label="Text"
            placeholder="privet"
            caption="variant=primary; failed=true; size=m; loading=true; errorerrorerrorer rorerrorerrorerro rerrorerrorerrorerror"
            limitedSymbols={5000}
            loading
            size="m"
            id="textfield-5"
          />
        </Box>
        <Box>
          <TextField
            value={'TextTextTextTextTextTextTextTextTextText'}
            label="Text"
            placeholder="place"
            limitedSymbols={5000}
            multiline
            caption="variant=primary; multiline=true; size=l"
            size="l"
            id="textfield-6"
          />
        </Box>
      </Wrapper>
      <Wrapper>
        <TextField
          value={value}
          onChange={(event) => setValue(event.target.value)}
          required
          label="Text"
          placeholder="privet"
          caption="variant=primary; fullWidth=true; required=true"
          fullWidth
          id="textfield-7"
        />
      </Wrapper>
      <h2>Secondary</h2>
      <Wrapper>
        <Box>
          <TextField
            variant="secondary"
            readOnly
            value={'ReadOnly'}
            label="Text"
            caption="variant=secondary; readOnly=true"
            size="s"
            id="textfield-8"
          />
        </Box>
        <Box>
          <TextField
            variant="secondary"
            required
            label="Text"
            placeholder="placehholder Text"
            loading
            caption="variant=secondary; loading=true; readOnly=true required=true"
            limitedSymbols={250}
            value={'TextTextTextTextTextTextTextTextTextText'}
            readOnly
            size="m"
            id="textfield-9"
          />
        </Box>
        <Box>
          <TextField
            variant="secondary"
            value={'TextTextTextTextTextTextTextTextTextText'}
            label="Text"
            placeholder="privet"
            limitedSymbols={5000}
            size="l"
            id="textfield-10"
          />
        </Box>
      </Wrapper>
      <Wrapper>
        <Box style={{ alignItems: 'center', margin: '0' }}>
          <h3>Size: s</h3>
        </Box>
        <Box style={{ alignItems: 'center', margin: '0' }}>
          <h3>Size: m</h3>
        </Box>
        <Box style={{ alignItems: 'center', margin: '0' }}>
          <h3>Size: l</h3>
        </Box>
      </Wrapper>
      <Wrapper>
        <Box>
          <TextField
            variant="secondary"
            value={value}
            onChange={(event) => setValue(event.target.value)}
            caption="variant=secondary; failed=true; size=s disabled=true"
            failed
            label="Text"
            placeholder="disabled"
            limitedSymbols={5000}
            size="s"
            disabled
            id="textfield-11"
          />
        </Box>
        <Box>
          <TextField
            variant="secondary"
            value={value}
            onChange={(event) => setValue(event.target.value)}
            failed
            label="Text"
            placeholder="privet"
            caption="variant=secondary; failed=true; size=m; loading=true; errorerrorerrorer rorerrorerrorerro rerrorerrorerrorerror"
            limitedSymbols={5000}
            loading
            size="m"
            id="textfield-12"
          />
        </Box>
        <Box>
          <TextField
            variant="secondary"
            value={'TextTextTextTextTextTextTextTextTextText'}
            label="Text"
            placeholder="place"
            limitedSymbols={5000}
            multiline
            caption="variant=secondary; multiline=true; size=l"
            size="l"
            id="textfield-13"
          />
        </Box>
      </Wrapper>
      <Wrapper>
        <TextField
          variant="secondary"
          value={value}
          onChange={(event) => setValue(event.target.value)}
          required
          label="Text"
          placeholder="privet"
          caption="variant=secondary; fullWidth=true"
          fullWidth
          id="textfield-14"
        />
      </Wrapper>
    </Container>
  );
};

export default ApplicationPage;
