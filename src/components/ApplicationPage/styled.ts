import styled from 'styled-components';

export const Container = styled.div({
  display: 'flex',
  width: '100dvw',
  minHeight: '100dvh',
  height: '100%',
  flexDirection: 'column',
  alignItems: 'center',
});

export const Wrapper = styled.div({
  display: 'flex',
  width: '1000px',
  height: '100%',
  justifyContent: 'space-between',
  margin: '25px 0',
});

export const Box = styled.div({
  display: 'flex',
  width: '300px',
  height: 'auto',
  flexDirection: 'column',
});
